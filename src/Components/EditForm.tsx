function EditForm() {

}

export default EditForm;

/**
 * <div className="row justify-content-center mt-5">
            <div className="col-6">
                <form id="updateForm">
                    <div className="form-row">
                        <div className="form-group col-md-12 mb-2">
                            <label htmlFor="firstname">First Name</label>
                            <input type="text" className="form-control" name="firstname" required />
                            <label htmlFor="lastname">Last Name</label>
                            <input type="text" className="form-control" name="lastname" required />
                        </div>
                        <div className="form-group col-md-12 mb-2">
                            <label htmlFor="physical_address">Physical Address</label>
                            <input className="form-control" name="physical_address" required />
                            <label htmlFor="billing_address">Billing Address</label>
                            <input type="text" className="form-control" name="billing_address" required />
                        </div>
                    </div>
                    <br />
                    <div className="d-flex justify-content-between">
                        <a href="/" className="btn btn-sm btn btn-success" id="back">Back</a>
                        <button id="update" type="submit" className="btn btn-sm btn btn-success">Update</button>
                    </div>
                </form>
            </div>
    </div>
 */