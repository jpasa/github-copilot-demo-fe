function CreateForm() {

}

export default CreateForm;

/**
 * <div className="row justify-content-center mt-5">
            <div className="col-6">
                <form id="createForm">
                    <div className="form-row">
                        <div className="form-group col-md-12 mb-2">
                            <label htmlFor="firstname">First Name</label>
                            <input type="text" className="form-control" name="firstname" required />
                            <label htmlFor="lastname">Last Name</label>
                            <input type="text" className="form-control" name="lastname" required />
                        </div>
                        <div className="form-group col-md-12 mb-2">
                            <label htmlFor="address">Address</label>
                            <input type="text" className="form-control" name="physical_address"
                                placeholder="Enter physical address" required /><br />
                            <input type="text" className="form-control" name="billing_address"
                                placeholder="Enter billing address" required />
                        </div>
                    </div>
                    <br />
                    <div className="d-flex justify-content-between">
                        <a href="/" className="btn btn-sm btn btn-dark" id="back">Back</a>
                        <button type="submit" className="btn btn-sm btn btn-dark" id="create">Create</button>
                    </div>
                </form>
            </div>
        </div>
 */