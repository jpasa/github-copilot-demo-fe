# User Contact Management System

This is the frontend project for the User Contact Management System. It is a full-stack MERN application where you are able to manage users contact information by performing Create, Read, Update & Delete operations with the users from MongoDB. 

## Requirements
- Before you begin make sure your development environment includes [Node.js](https://nodejs.org/en/download/) and NPM.
- Make sure to clone & start the backend repository of the project [here](https://gitlab.com/jpasa/demo/-/tree/github-copilot-be?ref_type=heads)

## Local Development
- Clone the project either using SSH or HTTPS. 
- Run a bash/cmd terminal and run the installation command `npm install`
- Run `npm start`